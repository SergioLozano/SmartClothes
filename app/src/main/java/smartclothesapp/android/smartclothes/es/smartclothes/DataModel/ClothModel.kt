package smartclothesapp.android.smartclothes.es.smartclothes.DataModel

import android.util.Log
import com.google.firebase.database.*
import java.util.*
import kotlin.collections.ArrayList

object ClothModel: Observable() {
    private var mValueDataListener: ValueEventListener? = null
    private var mClothList: ArrayList<Cloth>? = ArrayList()

    private fun getDatabaseRef(): DatabaseReference? {
        return FirebaseDatabase.getInstance().reference.child("clothes")

    }

    init {
        if(mValueDataListener != null){
            getDatabaseRef()?.removeEventListener(mValueDataListener)
        }

        mValueDataListener = null
        Log.i("ClothModel", "Data init")

        mValueDataListener = object: ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
                if(p0 != null) {
                    Log.i("ClothModel", "Data update cancelled. error = ${p0.message}")
                }
            }

            override fun onDataChange(dataSnapchot: DataSnapshot?) {
                try {
                    Log.i("ClothModel", "Data updated")
                    val data: ArrayList<Cloth> = ArrayList()
                    if(dataSnapchot != null){
                        for (snapshot: DataSnapshot in dataSnapchot.children){
                            try{
                                data.add(Cloth(snapshot))
                            }catch (e: Exception){
                                e.printStackTrace()
                            }
                        }
                        mClothList = data
                        Log.i("ClothModel", "Data updated, there are " + mClothList!!.size + "in the cache")
                        setChanged()
                        notifyObservers()
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }
        }
        getDatabaseRef()?.addValueEventListener(mValueDataListener)
    }

    fun getData(): ArrayList<Cloth>? {
        return mClothList
    }
}