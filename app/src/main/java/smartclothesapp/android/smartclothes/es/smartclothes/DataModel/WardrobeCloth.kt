package smartclothesapp.android.smartclothes.es.smartclothes.DataModel

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class WardrobeCloth {

    var id: Long? = null
    var status: Int = 0




    constructor() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    constructor(id: Long?, status: Int) {
        this.id = id
        this.status = status
    }
}