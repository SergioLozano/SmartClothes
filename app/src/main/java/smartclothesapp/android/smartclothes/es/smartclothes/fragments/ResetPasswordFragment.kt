package smartclothesapp.android.smartclothes.es.smartclothes.fragments

import android.app.AlertDialog
import android.graphics.Typeface
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import org.jetbrains.anko.support.v4.alert
import smartclothesapp.android.smartclothes.es.smartclothes.Utils.EditTextClearCross
import smartclothesapp.android.smartclothes.es.smartclothes.R


class ResetPasswordFragment : Fragment() {

    private var mAuth: FirebaseAuth? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_forgot_password, container, false)

        val etEmail = view.findViewById<EditTextClearCross>(R.id.et_outfit_name)
        etEmail.requestFocus()

        Log.d("Count signin",fragmentManager?.backStackEntryCount.toString())

        mAuth = FirebaseAuth.getInstance()

        val progressBar = view.findViewById<ProgressBar>(R.id.add_clothes_progressBar)
        val layout = view.findViewById<ConstraintLayout>(R.id.reset_password_constraint_layout)
        val signInBtn = view.findViewById<Button>(R.id.to_sign_in_btn)
        val resetPassBtn = view.findViewById<Button>(R.id.reset_password_btn)

        resetPassBtn.setOnClickListener {
            val userMail = etEmail?.text.toString().trim()
            layout.visibility = View.GONE
            progressBar.visibility = View.VISIBLE
            if(!TextUtils.isEmpty(userMail) && android.util.Patterns.EMAIL_ADDRESS.matcher(userMail).matches()){
                mAuth?.sendPasswordResetEmail(userMail)?.addOnCompleteListener { task ->
                    if(task.isSuccessful) {
                        progressBar.visibility = View.GONE
                        val dialog = alert("Check your email for instructions on how to reset your password."){
                            title = "Reset password"
                            positiveButton("OK") {signIn()}
                        }.show()
                        val textView = dialog.findViewById<TextView>(android.R.id.message)
                        val typeFace = Typeface.createFromAsset(context?.assets, "fonts/SourceSansPro-Regular.ttf")
                        textView.typeface = typeFace
                    }
                }
            }else{
                progressBar.visibility = View.GONE
                layout.visibility = View.VISIBLE
                val dialog = AlertDialog.Builder(context)
                        .setTitle("Account name")
                        .setMessage("Your SmartClothes Account name needs to be a working email address.")
                        .setPositiveButton("OK", null)
                        .show()
                val textView = dialog.findViewById<TextView>(android.R.id.message)
                val typeFace = Typeface.createFromAsset(context?.assets, "fonts/SourceSansPro-Regular.ttf")
                textView.typeface = typeFace
            }

        }
        signInBtn.setOnClickListener{
            signIn()
        }
        return view

    }

    private fun signIn(){
        val fragmentManager = fragmentManager
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
        fragmentTransaction.addToBackStack(null)
        val signInFragment = SignInFragment()
        fragmentTransaction.replace(R.id.sesionFragments, signInFragment)
        fragmentTransaction.commit()
    }


}
