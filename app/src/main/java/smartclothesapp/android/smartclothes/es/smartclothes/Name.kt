package smartclothesapp.android.smartclothes.es.smartclothes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import smartclothesapp.android.smartclothes.es.smartclothes.Utils.EditTextClearCross


class Name : AppCompatActivity() {

    private lateinit var nameEt: EditTextClearCross
    private val mAuth = FirebaseAuth.getInstance()
    private val mRef = FirebaseDatabase.getInstance().reference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_name)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        nameEt = findViewById(R.id.name_et)

        nameEt.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            // If the event is a key-down event on the "enter" button
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                // Perform action on key press
                val user = mAuth.currentUser?.uid
                val nameTxt = nameEt.text.trim().toString()
                mRef.child("users").child(user).child("username").setValue(nameTxt)
                finish()
                return@OnKeyListener true
            }
            false
        })
    }


}
