package smartclothesapp.android.smartclothes.es.smartclothes.fragments

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import smartclothesapp.android.smartclothes.es.smartclothes.Adapters.ClothesAdapter
import smartclothesapp.android.smartclothes.es.smartclothes.R
import android.util.TypedValue

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Clothes
import kotlin.collections.ArrayList
import com.google.firebase.database.DataSnapshot
import org.jetbrains.anko.support.v4.startActivity
import smartclothesapp.android.smartclothes.es.smartclothes.AddClothes

import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.WardrobeCloth


class ClothesFragment : Fragment(){

    private var mClothListAdapter: ClothesAdapter? = null
    private var mContext: Context? = null
    private var recycler: RecyclerView? = null
    private var mUserClothes: ArrayList<Clothes?> = ArrayList()
    private lateinit var noClothesTv: TextView
    private lateinit var mProgressBar: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_closet_clothes, container, false)
        noClothesTv = view.findViewById(R.id.no_clothes_tv)
        mProgressBar = view.findViewById(R.id.status_progressBar)

        noClothesTv.setOnClickListener { startActivity<AddClothes>() }
        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        getUserClothes()
    }





    private fun init(){
        recycler = view?.findViewById(R.id.clothes_recyclerView)
        val mLayoutManager = GridLayoutManager(mContext, 2)
        recycler?.layoutManager = mLayoutManager
        recycler?.addItemDecoration(GridSpacingItemDecoration(2, dpToPx(10), true))
        mClothListAdapter = ClothesAdapter(activity, mUserClothes, R.layout.cloth_card)
        recycler?.adapter = mClothListAdapter


    }



    private fun getUserClothes(){
        val user = FirebaseAuth.getInstance().currentUser?.uid
        val mRef = FirebaseDatabase.getInstance().reference.child("wardrobe").child(user).child("clothes")
        val userClothsList = ArrayList<WardrobeCloth?>()
        mProgressBar.visibility = View.VISIBLE
        noClothesTv.visibility = View.GONE
        mRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(snap: DataSnapshot?) {
                userClothsList.clear()
                if (!snap!!.exists()){
                    mProgressBar.visibility = View.GONE
                    noClothesTv.visibility = View.VISIBLE
                }
                for(data: DataSnapshot in snap.children){
                    val cloth = data.getValue(WardrobeCloth::class.java)

                    userClothsList.add(cloth)


                    compareUserClothes(userClothsList)
                }
            }

        })
    }

    private fun compareUserClothes(userClothes: ArrayList<WardrobeCloth?>){
        val mRef = FirebaseDatabase.getInstance().reference.child("clothes")

        mRef.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(snap: DataSnapshot?) {
                mUserClothes.clear()
                for (data: DataSnapshot in snap!!.children){
                    val cloth = data.getValue(Clothes::class.java)

                    for(getCloth: WardrobeCloth? in userClothes){
                        if(getCloth!!.id == cloth!!.id){
                            mUserClothes.add(cloth)

                            if (mUserClothes.isNotEmpty()){
                                noClothesTv.visibility = View.GONE
                            }else{
                                mProgressBar.visibility = View.GONE
                                noClothesTv.visibility = View.VISIBLE
                            }
                        }


                    }
                    mProgressBar.visibility = View.GONE
                    mClothListAdapter?.notifyDataSetChanged()

                }


            }

        })
    }






    inner class GridSpacingItemDecoration(spanCount:Int, spacing:Int, includeEdge:Boolean):RecyclerView.ItemDecoration() {
        private var spanCount:Int = 0
        private var spacing:Int = 0
        private var includeEdge:Boolean = false

        init{
            this.spanCount = spanCount
            this.spacing = spacing
            this.includeEdge = includeEdge
        }

        override fun getItemOffsets(outRect: Rect, view:View, parent:RecyclerView, state:RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column
            if (includeEdge)
            {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)
                if (position < spanCount)
                { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            }
            else
            {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f / spanCount) * spacing)
                if (position >= spanCount)
                {
                    outRect.top = spacing // item top
                }
            }
        }
    }

    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics))
    }



}