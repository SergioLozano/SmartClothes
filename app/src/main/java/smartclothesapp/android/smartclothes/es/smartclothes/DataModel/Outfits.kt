package smartclothesapp.android.smartclothes.es.smartclothes.DataModel

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Outfits {
    lateinit var id: String
    lateinit var clothes:ArrayList<Long>
    lateinit var name: String

    constructor() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    constructor(id: String, name: String, clothes: ArrayList<Long>) {
        this.id = id
        this.name = name
        this.clothes = clothes
    }
}