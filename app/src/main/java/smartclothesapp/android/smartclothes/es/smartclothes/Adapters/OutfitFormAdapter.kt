package smartclothesapp.android.smartclothes.es.smartclothes.Adapters

import android.content.Context
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.squareup.picasso.Picasso

import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Clothes
import java.nio.file.Files.size





class OutfitFormAdapter(val mContext: Context?, val mClothes: ArrayList<Clothes?>, val layout: Int, val mListClothes: ArrayList<String>):RecyclerView.Adapter<OutfitFormAdapter.ViewHolder>() {

   private val clothes: List<Clothes?> = mClothes

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = clothes[position]
        holder.name.text = list?.name

        Picasso.with(mContext).load(list?.image).into(holder.image)

        holder.removeBtn.setOnClickListener {
            mListClothes.remove(list!!.id.toString())
            mClothes.removeAt(position)
            notifyItemRemoved(position)
            notifyItemRangeChanged(position, mClothes.size)
        }

    }

    override fun getItemCount(): Int {
        return mClothes.size
    }


    inner class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView
        var image: ImageView
        var removeBtn: Button


        init {
            name = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.cloth_name)
            image = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.cloth_image)
            removeBtn = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.remove_cloth_btn)

        }
    }


}


