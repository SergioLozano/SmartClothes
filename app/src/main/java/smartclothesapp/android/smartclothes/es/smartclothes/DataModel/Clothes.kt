package smartclothesapp.android.smartclothes.es.smartclothes.DataModel

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Clothes {
    var id: Long = 0
    lateinit var name: String
    lateinit var image: String
    lateinit var description: String
    lateinit var category: String



        constructor() {
            // Default constructor required for calls to DataSnapshot.getValue(User.class)
        }

        constructor(id: Long, name: String, image: String, description: String, category: String) {
            this.id = id
            this.name = name
            this.image = image
            this.description = description
            this.category = category
        }

}