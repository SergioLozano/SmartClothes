package smartclothesapp.android.smartclothes.es.smartclothes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import smartclothesapp.android.smartclothes.es.smartclothes.Utils.EditTextClearCross

class DeleteAccount : AppCompatActivity() {

    private val mAuth = FirebaseAuth.getInstance()
    private val mRef = FirebaseDatabase.getInstance().reference

    private lateinit var deleteBtn: Button
    private lateinit var emailEt: EditTextClearCross
    private lateinit var passEt: EditTextClearCross

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delete_account)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        deleteBtn = findViewById(R.id.delete_account_btn)
        emailEt = findViewById(R.id.email_et)
        passEt = findViewById(R.id.password_et)

        deleteBtn.setOnClickListener { deleteAccount() }
    }

    private fun deleteAccount(){

        val user = mAuth.currentUser?.uid
        val userMail = emailEt.text.toString().trim()
        val userPass = passEt.text.toString().trim()

        if(userMail.isEmpty() || userPass.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(userMail).matches()){
            alert (getString(R.string.invalid_data)){
                title = getString(R.string.error)
                positiveButton("OK"){}
            }.show()
        }else{
            val credential = EmailAuthProvider.getCredential(userMail, userPass)
            mAuth.currentUser?.reauthenticate(credential)?.addOnSuccessListener {
                mRef.child("users").child(user).removeValue().addOnCompleteListener {
                    mAuth.currentUser?.delete()?.addOnCompleteListener { task ->
                        if (task.isSuccessful){
                            startActivity<SessionActivity>()
                            finish()
                        }else{
                            Toast.makeText(applicationContext, getString(R.string.error), Toast.LENGTH_SHORT).show()
                        }

                    }
                }

            }
        }
    }
}


