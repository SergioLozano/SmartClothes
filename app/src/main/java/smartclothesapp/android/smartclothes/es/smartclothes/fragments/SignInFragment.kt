package smartclothesapp.android.smartclothes.es.smartclothes.fragments

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.common.SignInButton
import smartclothesapp.android.smartclothes.es.smartclothes.R
import android.widget.Button
import android.content.Intent
import android.graphics.Typeface
import android.os.AsyncTask
import android.support.constraint.ConstraintLayout
import android.text.TextUtils
import android.widget.Toast
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import smartclothesapp.android.smartclothes.es.smartclothes.Utils.EditTextClearCross
import smartclothesapp.android.smartclothes.es.smartclothes.MainActivity
import smartclothesapp.android.smartclothes.es.smartclothes.SessionActivity
import android.support.design.widget.Snackbar
import android.widget.TextView
import com.google.firebase.auth.*
import com.google.firebase.database.FirebaseDatabase
import org.jetbrains.anko.support.v4.indeterminateProgressDialog
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.User


class SignInFragment : Fragment() {

    var etEmail: EditTextClearCross? = null
    var etPassword: EditTextClearCross? = null

    lateinit var mGoogleSignInClient: GoogleSignInClient
    lateinit var gso: GoogleSignInOptions
    private val RC_SIGN_IN: Int = 3004

    private val toastDuration = Toast.LENGTH_LONG

    private val mOnClickListener = View.OnClickListener { view ->
        val id = view.id
        when (id) {
            R.id.sign_in_btn -> signIn()
            R.id.forgot_password_btn -> forgotPass()
            R.id.sign_up_btn -> signUp()
            R.id.google_sign_in_btn -> signInGoogle()

        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_sign_in, container, false)

        // Set the dimensions of the sign-in button.
        val googleSignInBtn = view.findViewById<SignInButton>(R.id.google_sign_in_btn)
        googleSignInBtn?.setSize(SignInButton.SIZE_WIDE)

        val signInBtn = view.findViewById<Button>(R.id.sign_in_btn)
        val signUpBtn = view.findViewById<Button>(R.id.sign_up_btn)
        val forgotPassworddBtn = view.findViewById<Button>(R.id.forgot_password_btn)

        signInBtn.setOnClickListener(mOnClickListener)
        signUpBtn.setOnClickListener(mOnClickListener)
        forgotPassworddBtn.setOnClickListener(mOnClickListener)
        googleSignInBtn.setOnClickListener(mOnClickListener)


        etEmail = view?.findViewById(R.id.et_outfit_name)
        etPassword = view?.findViewById(R.id.et_password)

        etEmail?.requestFocus()

        gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient((activity as SessionActivity), gso)

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        etEmail = view?.findViewById(R.id.et_outfit_name)
    }

    private fun signInGoogle() {
        val signInIntent: Intent = mGoogleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RC_SIGN_IN) {
           val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try{
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account)
            }catch (e: ApiException){

            }
        }
    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount) {
        val mAuth = FirebaseAuth.getInstance()
        val db = FirebaseDatabase.getInstance().reference
        val credential = GoogleAuthProvider.getCredential(account.idToken, null)

        mAuth?.signInWithCredential(credential)
                ?.addOnCompleteListener ((activity as SessionActivity),  { task ->
                    if (task.isSuccessful){

                        val user = mAuth.currentUser
                        val id = user?.uid
                        val name = user?.displayName
                        val email = user?.email
                        val verified = user?.isEmailVerified
                        val photoUrl: String

                        photoUrl = if(user?.photoUrl != null){
                            user.photoUrl.toString()
                        }else{
                            "null"
                        }


                        val userData = User(id!!, name!!, email!!, verified!!, photoUrl)


                        db.child("users").child(id).setValue(userData)

                        val intent = Intent(activity, MainActivity::class.java)
                        startActivity(intent)

                    } else {
                        Snackbar.make(view?.findViewById<ConstraintLayout>(R.id.session_activity_layout)!!, "Authentication Failed.", Snackbar.LENGTH_SHORT).show()
                    }
                })
    }

    private fun signIn() {
        val mAuth = FirebaseAuth.getInstance()

        val userMail = etEmail?.text.toString().trim()
        val userPass = etPassword?.text.toString().trim()

        if(!TextUtils.isEmpty(userMail) && android.util.Patterns.EMAIL_ADDRESS.matcher(userMail).matches() && !TextUtils.isEmpty(userPass)) {
            mAuth?.signInWithEmailAndPassword(userMail, userPass)
                    ?.addOnCompleteListener ((activity as SessionActivity),  { task ->
                        if (task.isSuccessful){
                            val intent = Intent(activity, MainActivity::class.java)
                            startActivity(intent)
                        } else {
                            dialog()
                        }
                    })
        } else {
            dialog()
        }

    }

    private fun forgotPass() {
        val fragmentManager = fragmentManager
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
        fragmentTransaction.addToBackStack(null)
        val resetPasswordFragment = ResetPasswordFragment()
        fragmentTransaction.replace(R.id.sesionFragments, resetPasswordFragment)
        fragmentTransaction.commit()
    }

    private fun signUp() {
        val fragmentManager = fragmentManager
        val fragmentTransaction = fragmentManager!!.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
        fragmentTransaction.addToBackStack(null)
        val signupFragment = SignUpFragment()
        fragmentTransaction.replace(R.id.sesionFragments, signupFragment)
        fragmentTransaction.commit()
    }

    private fun dialog () {
        val dialog = AlertDialog.Builder(context)
                .setTitle("Account name")
                .setMessage("Your SmartClothes Account name needs to be a working email address.")
                .setPositiveButton("OK", null)
                .show()
        val textView = dialog.findViewById<TextView>(android.R.id.message)
        val typeFace = Typeface.createFromAsset(context?.assets, "fonts/SourceSansPro-Regular.ttf")
        textView.typeface = typeFace
    }

    internal inner class saveUserData: AsyncTask<Void, Void, Void>(){
        private val dialog = indeterminateProgressDialog(getString(R.string.checking_clothes))

        override fun onPostExecute(result: Void?) {
            super.onPostExecute(result)
            dialog.dismiss()
        }

        override fun doInBackground(vararg params: Void?): Void? {

            (activity as SessionActivity).saveUser()

            return null
        }


        override fun onPreExecute() {
            super.onPreExecute()
            dialog.show()
        }
    }


}
