package smartclothesapp.android.smartclothes.es.smartclothes.Adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.startActivity
import smartclothesapp.android.smartclothes.es.smartclothes.Closet
import smartclothesapp.android.smartclothes.es.smartclothes.ClothDetail

import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Clothes
import smartclothesapp.android.smartclothes.es.smartclothes.R


class ClothesAdapter(val mContext: Context?, val mClothes: ArrayList<Clothes?>, val layout: Int):RecyclerView.Adapter<ClothesAdapter.ViewHolder>() {

   private val clothes: List<Clothes?> = mClothes

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val list = clothes[position]
        val tagId = list?.id.toString()
        holder.title.text = list?.name
        holder.count.text = tagId

        Picasso.with(mContext).load(list?.image).into(holder.thumb)

        holder.thumb.setOnClickListener {
            val intent = Intent(mContext, ClothDetail::class.java)
            intent.putExtra("CLOTH_ID", list?.id)
            mContext!!.startActivity(intent)
        }

        holder.overflow.setOnClickListener { showPopupMenu(holder.overflow, tagId, position) }
    }

    override fun getItemCount(): Int {
        return mClothes.size
    }


    inner class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        var title: TextView
        var count: TextView
        var thumb: ImageView
        var overflow: ImageView

        init {
            title = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.title)
            count = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.count)
            thumb = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.thumbnail)
            overflow = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.overflow)
        }
    }

    private fun showPopupMenu(view: View, cloth:String, position: Int) {
        // inflate menu
        val popup = PopupMenu(mContext!!, view)
        val inflater = popup.menuInflater
        inflater.inflate(smartclothesapp.android.smartclothes.es.smartclothes.R.menu.menu_closet, popup.menu)
        popup.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { menuItem ->
            when(menuItem.itemId){
                smartclothesapp.android.smartclothes.es.smartclothes.R.id.action_remove_cloth -> {
                    removeFromCloset(cloth, position)
                    return@OnMenuItemClickListener true
                }
            }
            false
        })
        popup.show()
    }

    private fun removeFromCloset(cloth: String, position: Int){
        val db = FirebaseDatabase.getInstance().reference
        val mAuth = FirebaseAuth.getInstance()
        val user = mAuth.currentUser?.uid
        val dbRef = db.child("wardrobe").child(user).child("clothes").child(cloth)
        dbRef.removeValue().addOnCompleteListener {task ->
            if (task.isSuccessful) {
                Toast.makeText(mContext, "Removing cloth", Toast.LENGTH_SHORT).show()
                mClothes.removeAt(position)
                notifyItemRemoved(position)
                notifyItemRangeChanged(position, mClothes.size)

            } else {
                Toast.makeText(mContext, "Error can't remove this cloth", Toast.LENGTH_SHORT).show()
            }
        }
    }






}


