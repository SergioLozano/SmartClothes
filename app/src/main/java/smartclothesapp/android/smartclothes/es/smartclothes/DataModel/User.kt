package smartclothesapp.android.smartclothes.es.smartclothes.DataModel

import android.net.Uri
import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class User {

    lateinit var id: String
    lateinit  var username: String
    lateinit var email: String
    lateinit var photoUrl: String
    var verified: Boolean = false



    constructor() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    constructor(id: String, username: String, email: String, verified: Boolean, photoUrl: String) {
        this.id = id
        this.username = username
        this.email = email
        this.verified = verified
        this.photoUrl = photoUrl
    }

}