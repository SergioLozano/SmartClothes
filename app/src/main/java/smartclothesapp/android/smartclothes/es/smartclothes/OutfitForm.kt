package smartclothesapp.android.smartclothes.es.smartclothes

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.Button
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import smartclothesapp.android.smartclothes.es.smartclothes.Adapters.OutfitFormAdapter
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Clothes
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Outfits
import smartclothesapp.android.smartclothes.es.smartclothes.Utils.EditTextClearCross

class OutfitForm : AppCompatActivity() {

    private var recycler: RecyclerView? = null
    private lateinit var outfitNameEt: EditTextClearCross
    private lateinit var deleteBtn: Button


    private var mUserClothes: ArrayList<Clothes?> = ArrayList()
    private var mClothListAdapter: OutfitFormAdapter? = null

    private lateinit var position: String
    private lateinit var name: String
    private var clothes: ArrayList<String> = ArrayList()

    private val mAuth = FirebaseAuth.getInstance()
    private val mRef = FirebaseDatabase.getInstance().reference

    private var  saving = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outfit_form)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        outfitNameEt = findViewById(R.id.et_outfit_name)
        deleteBtn = findViewById(R.id.delete_outfit_btn)
        val addClothesBtn = findViewById<Button>(R.id.add_cloth_btn)

        val user = mAuth.currentUser?.uid


        val extras = intent.extras
        if (extras != null){
            position = intent.getStringExtra("position")
            name = intent.getStringExtra("name")
            clothes = intent.getStringArrayListExtra("clothes")

            outfitNameEt.setText(name)
        }else{
            position = "0"
        }



        init()

        compareUserClothes()

        addClothesBtn.setOnClickListener {
            name = outfitNameEt.text.trim().toString()
            val intent = Intent(this, PickCloth::class.java)
            intent.putExtra("position", position)
            intent.putExtra("name", name)
            intent.putExtra("clothes", clothes)
            startActivity(intent)
        }

        deleteBtn.setOnClickListener {
            val query = mRef.child("wardrobe").child(user).child("outfits")
            query.orderByChild("id").equalTo(position).addListenerForSingleValueEvent(object: ValueEventListener{
                override fun onCancelled(p0: DatabaseError?) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(p0: DataSnapshot?) {
                   for (child: DataSnapshot in p0!!.children){
                       val key = child.key
                       query.child(key).removeValue().addOnCompleteListener {
                           val intent = Intent(applicationContext, Closet::class.java)
                           startActivity(intent)
                       }
                   }
                }

            })


        }



    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_outfit_form, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if(item?.itemId == R.id.action_save){
            name = outfitNameEt.text.trim().toString()

            if(name.isEmpty() || clothes.isEmpty()){
                alert(getString(R.string.save_error_desc)) {
                    title = getString(R.string.error)
                    positiveButton("OK") { }
                }.show()
            }else {

                val user = mAuth.currentUser?.uid


                val query = mRef.child("wardrobe").child(user).child("outfits")

                query.addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError?) {
                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                    }

                    override fun onDataChange(p0: DataSnapshot?) {
                        val pos = p0!!.childrenCount

                        if (!saving) {
                            pushValues(pos)
                        }

                    }

                })

            }

        }
        return super.onOptionsItemSelected(item)
    }

    private fun pushValues(pos: Long){
        saving = true
        val clothesLong = ArrayList<Long>()
        val user = mAuth.currentUser?.uid
        for (s:String in clothes){
            clothesLong.add(s.toLong())
        }


        val outfit = Outfits(pos.toString(), name, clothesLong)
        mRef.child("wardrobe").child(user).child("outfits").push().setValue(outfit).addOnCompleteListener {
            startActivity<Closet>()

        }
    }

    private fun init(){
        recycler = findViewById(R.id.picked_clothes_recycler)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        recycler?.layoutManager = mLayoutManager
        mClothListAdapter = OutfitFormAdapter(this, mUserClothes, R.layout.outfit_selected_clothes_card, clothes)
        recycler?.adapter = mClothListAdapter
    }

    private fun compareUserClothes(){
        val mRef = FirebaseDatabase.getInstance().reference.child("clothes")

        mRef.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(snap: DataSnapshot?) {
                mUserClothes.clear()
                for (data: DataSnapshot in snap!!.children){
                    val cloth = data.getValue(Clothes::class.java)

                    for(getCloth: String in clothes){
                        if(getCloth == cloth?.id.toString()){
                            mUserClothes.add(cloth)
                        }


                    }

                    mClothListAdapter?.notifyDataSetChanged()

                }


            }

        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity<Closet>()
    }


}
