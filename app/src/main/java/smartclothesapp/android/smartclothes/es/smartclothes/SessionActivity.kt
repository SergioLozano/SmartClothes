package smartclothesapp.android.smartclothes.es.smartclothes

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Fragment
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import smartclothesapp.android.smartclothes.es.smartclothes.fragments.SignInFragment
import android.graphics.Rect
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import android.content.Intent
import android.content.res.Configuration
import android.support.constraint.ConstraintLayout
import android.transition.*
import android.util.DisplayMetrics
import android.util.Log
import android.view.animation.AnimationUtils
import android.view.animation.TranslateAnimation
import android.widget.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import android.util.TypedValue
import android.view.*
import android.view.animation.AnimationSet
import android.view.inputmethod.EditorInfo
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import org.jetbrains.anko.support.v4.viewPager
import java.util.HashMap


class SessionActivity : AppCompatActivity() {
    var logo: ImageView? = null

    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session)
        mAuth = FirebaseAuth.getInstance()
        loadFragment(SignInFragment())


    }

    override fun onStart() {
        super.onStart()
        val currentUser = mAuth?.currentUser

        if(currentUser != null){
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
    }

    private fun loadFragment(fragment: SignInFragment) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.sesionFragments, fragment)
        ft.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        val fragment = supportFragmentManager.findFragmentById(R.id.sesionFragments)
        fragment?.onActivityResult(requestCode, resultCode, data)
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }


    fun saveUser(){
        val user = mAuth?.currentUser

        val taskMap = HashMap<String, Any?>()
        taskMap["id"] = user?.uid
        taskMap["email"] = user?.email
        Log.d("Bibop", user?.uid)
        //db.child("users").child(user?.uid).setValue(taskMap)
    }


    /* fun animation() {
         activityContainer = findViewById(R.id.session_activity_layout)
         logo = findViewById(R.id.small_logo)
         val set = ChangeBounds()
         set.duration = 300
         TransitionManager.beginDelayedTransition(activityContainer, set)
         if(logo?.visibility == View.GONE) logo?.visibility = View.VISIBLE else logo?.visibility = View.GONE
     }*/

    override fun onBackPressed() {
        val signInFragment = findViewById<FrameLayout>(R.id.sigin_fragment_framelayout)
        if(signInFragment?.visibility == View.VISIBLE) {

            //animation()
            this.moveTaskToBack(true)

        }else{
            super.onBackPressed()


        }

    }


    /*override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {

        if(keyCode == KeyEvent.KEYCODE_BACK) {
            animation()
            return true
        }

        return super.onKeyDown(keyCode, event)
    }*/




}
