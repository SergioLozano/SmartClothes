package smartclothesapp.android.smartclothes.es.smartclothes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.User

class ManageAccount : AppCompatActivity() {

    private lateinit var name: TextView
    private lateinit var email: TextView

    private lateinit var nameBtn: LinearLayout
    private lateinit var emailBtn: LinearLayout
    private lateinit var securityBtn: LinearLayout
    private lateinit var deleteAccountBtn: Button

    private val mAuth = FirebaseAuth.getInstance()
    private val mRef = FirebaseDatabase.getInstance().reference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_account)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        nameBtn = findViewById(R.id.name_btn)
        emailBtn = findViewById(R.id.email_btn)
        securityBtn = findViewById(R.id.password_btn)
        deleteAccountBtn = findViewById(R.id.delete_account_btn)

        val user = mAuth.currentUser?.uid

        mRef.child("users").child(user).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(data: DataSnapshot?) {
                if (data!!.exists()) {
                    val userData = data.getValue(User::class.java)

                    if (userData?.username != "null") {
                        name.text = userData?.username
                    }

                    email.text = userData?.email
                }
            }

        })

        nameBtn.setOnClickListener { startActivity<Name>() }
        emailBtn.setOnClickListener { startActivity<Email>() }
        securityBtn.setOnClickListener { startActivity<AccountPassword>() }
        deleteAccountBtn.setOnClickListener { startActivity<DeleteAccount>() }


    }

}
