package smartclothesapp.android.smartclothes.es.smartclothes

import android.content.Intent
import android.graphics.Typeface
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.MifareUltralight
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.wrapContent
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Cloth
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.ClothModel
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.WardrobeCloth
import smartclothesapp.android.smartclothes.es.smartclothes.Utils.NfcUtils
import java.util.*


class AddClothes : AppCompatActivity(), NfcAdapter.ReaderCallback, Observer {


    private var nfcAdapter: NfcAdapter? = null
    private lateinit var image: ImageView
    private lateinit var progressBar: ProgressBar
    private lateinit var container: LinearLayout
    private lateinit var title: TextView
    private lateinit var description: TextView
    private lateinit var addBtn: Button

    private var clothList: ArrayList<Cloth>? = null
    private var tagId: Long? = null
    private var checkingTag: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_clothes)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        initViews()


        addBtn.setOnClickListener { addClothToWardrobe() }

        nfcAdapter = NfcAdapter.getDefaultAdapter(this)

        if (nfcAdapter == null) {
            val dialog = alert(getString(R.string.nfc_unavailable_description)) {
                title = getString(R.string.nfc_unavailable)
                positiveButton("OK") { startActivity<MainActivity>() }
            }.show()
            val textView = dialog.findViewById<TextView>(android.R.id.message)
            val typeFace = Typeface.createFromAsset(this.assets, "fonts/SourceSansPro-Regular.ttf")
            textView.typeface = typeFace
        }

        ClothModel

        ClothModel.addObserver(this)

    }

    private fun initViews() {
        image = findViewById(R.id.imageView)
        progressBar = findViewById(R.id.add_clothes_progressBar)
        title = findViewById(R.id.title)
        description = findViewById(R.id.description)
        container = findViewById(R.id.content_container)
        addBtn = findViewById(R.id.add_cloth_btn)
    }

    override fun onResume() {
        super.onResume()
        nfcAdapter?.enableReaderMode(this, this,
                NfcAdapter.FLAG_READER_NFC_A or NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK, null)
    }

    override fun update(o: Observable?, arg: Any?) {
        clothList?.clear()
        val data = ClothModel.getData()
        if (data != null) {
            clothList = data
        }
    }

    override fun onStop() {
        super.onStop()
        ClothModel.deleteObserver(this)
        finish()
    }


    override fun onPause() {
        super.onPause()
        nfcAdapter?.disableReaderMode(this)
        ClothModel.deleteObserver(this)
    }

    override fun onTagDiscovered(tag: Tag?) {

        if (!checkingTag) {

            checkingTag = true

            val miFareUltraLight = MifareUltralight.get(tag) //We only support mifare ultralight NFC tech

            try {
                miFareUltraLight.connect()

                val response: ByteArray = tag!!.id
                val hex = NfcUtils.toHex(response)
                val data = NfcUtils.toDec(hex)

                tagId = data

                runOnUiThread { getClothData(data) }

                miFareUltraLight.close()

            } catch (e: Exception) {
                e.printStackTrace()
                runOnUiThread { errorLoadingCloth() }
                //TODO: Handle other nfc techs errors
            }


        }

    }

    private fun getClothData(id: Long) {
        clothList = ClothModel.getData()
        var result = false
        var imageUrl: String? = null
        var name: String? = null
        val dialog = indeterminateProgressDialog(getString(R.string.checking_clothes))
        dialog.show()
        container.visibility = View.GONE
        addBtn.visibility = View.GONE
        for (cloth in clothList!!) {
            if (cloth.id == id.toString()) {
                result = true
                imageUrl = cloth.imageUrl
                name = cloth.name

            }
        }

        if (result) {
            Picasso.with(this)
                    .load(imageUrl)
                    .placeholder(R.drawable.ic_wifi_white)
                    .into(image, object : Callback {
                        override fun onSuccess() {
                            title.text = name

                            description.visibility = View.GONE
                            addBtn.visibility = View.VISIBLE
                            image.layoutParams.height = wrapContent
                            image.layoutParams.width = wrapContent
                            container.visibility = View.VISIBLE

                            dialog.dismiss()
                            checkingTag = false
                        }

                        override fun onError() {
                            dialog.dismiss()
                            checkingTag = false
                        }
                    })
        } else {
            errorLoadingCloth()
            dialog.dismiss()
        }
    }

    private fun errorLoadingCloth() {
        container.visibility = View.VISIBLE
        image.layoutParams.height = 180
        image.layoutParams.width = 180
        image.setImageDrawable(getDrawable(R.drawable.ic_sad_face))
        title.text = getString(R.string.undefined_cloth_title)
        description.visibility = View.VISIBLE
        description.text = getString(R.string.undefined_cloth)
        checkingTag = false
    }

    private fun addClothToWardrobe() {
        val db = FirebaseDatabase.getInstance().reference
        val mAuth = FirebaseAuth.getInstance()
        val user = mAuth.currentUser?.uid
        val dialog = indeterminateProgressDialog(getString(R.string.adding_cloth_to_wardrobe))
        dialog.show()
        val dbRef = db.child("wardrobe").child(user).child("clothes").child(tagId.toString())
        val wardRobeCloth = WardrobeCloth(tagId, 0)
        dbRef.setValue(wardRobeCloth).addOnCompleteListener(this, { task ->
            if (task.isSuccessful) {
                dialog.dismiss()
                val intent = Intent(this, ClothDetail::class.java)
                intent.putExtra("CLOTH_ID", tagId)
                startActivity(intent)

            } else {
                dialog.dismiss()
                alert(getString(R.string.wardrobe_error)) {
                    title = getString(R.string.error)
                    positiveButton("OK") {}
                }.show()
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity<MainActivity>()
    }
}
