package smartclothesapp.android.smartclothes.es.smartclothes.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import smartclothesapp.android.smartclothes.es.smartclothes.Adapters.ClothesAdapter
import smartclothesapp.android.smartclothes.es.smartclothes.Adapters.ClothesStatusAdapter
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Clothes
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.WardrobeCloth
import smartclothesapp.android.smartclothes.es.smartclothes.R

class ClothesStatusFragment : Fragment() {

    private var mClothListAdapter: ClothesStatusAdapter? = null
    private var mContext: Context? = null
    private var recycler: RecyclerView? = null
    private var mUserClothes: ArrayList<Clothes?> = ArrayList()
    private lateinit var noClothesTv: TextView
    private lateinit var mProgressBar: ProgressBar
    private var dirtyCloth: Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_closet_status, container, false)

        noClothesTv = view.findViewById(R.id.no_clothes_tv)
        mProgressBar = view.findViewById(R.id.status_progressBar)

        recycler = view.findViewById(R.id.status_recyclerView)

        val mLayoutManager = LinearLayoutManager(activity)
        recycler?.layoutManager = mLayoutManager

        mClothListAdapter = ClothesStatusAdapter(activity, R.layout.status_cloth_card)
        recycler?.adapter = mClothListAdapter
        recycler?.visibility = View.GONE

        getUserClothes()

        return view

    }

    override fun onDetach() {
        super.onDetach()

    }

    private fun getUserClothes(){
        val user = FirebaseAuth.getInstance().currentUser?.uid
        val mRef = FirebaseDatabase.getInstance().reference.child("wardrobe").child(user).child("clothes")
        val userClothsList = ArrayList<WardrobeCloth?>()
        mProgressBar.visibility = View.VISIBLE
        noClothesTv.visibility = View.GONE
        mRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(snap: DataSnapshot?) {
                val statusArray = ArrayList<Int?>()
                userClothsList.clear()
                statusArray.clear()

                if (!snap!!.exists()){
                    mProgressBar.visibility = View.GONE
                    noClothesTv.visibility = View.VISIBLE
                }
                for(data: DataSnapshot in snap.children){
                    val cloth = data.getValue(WardrobeCloth::class.java)


                    statusArray.add(cloth?.status)

                    showRecycler(statusArray)
                }
            }



        })

        mProgressBar.visibility = View.GONE
    }


    private fun showRecycler(statusArray: ArrayList<Int?>){
        if (statusArray.contains(1)) {
            noClothesTv.visibility = View.GONE
            recycler?.visibility = View.VISIBLE
        }else{
            noClothesTv.visibility = View.VISIBLE
            recycler?.visibility = View.GONE
        }
    }



}