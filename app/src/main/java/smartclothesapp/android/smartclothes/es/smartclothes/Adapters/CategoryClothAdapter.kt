package smartclothesapp.android.smartclothes.es.smartclothes.Adapters

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Clothes
import smartclothesapp.android.smartclothes.es.smartclothes.OutfitForm
import smartclothesapp.android.smartclothes.es.smartclothes.PickCloth


class CategoryClothAdapter (val mContext: Context?, val mClothes: ArrayList<Clothes?>,  val layout: Int, val mPosition: String, val mName: String, val mListClothes:ArrayList<String>): RecyclerView.Adapter<CategoryClothAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val list = mClothes[position]

        Picasso.with(mContext).load(list!!.image).into(holder.image)

        holder.image.setOnClickListener {

                mListClothes.add(list.id.toString())
                val intent = Intent(mContext, OutfitForm::class.java)
                intent.putExtra("position", mPosition)
                intent.putExtra("name", mName)
                intent.putExtra("clothes", mListClothes)
                mContext?.startActivity(intent)

        }



    }

    override fun getItemCount(): Int {
        return mClothes.size
    }


    inner class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        var image: ImageButton


        init {
            image = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.user_cloth_image)


        }
    }

}