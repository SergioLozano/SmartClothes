package smartclothesapp.android.smartclothes.es.smartclothes

import ai.api.AIConfiguration
import ai.api.AIDataService
import ai.api.AIServiceException

import ai.api.model.AIRequest
import ai.api.model.AIResponse
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.speech.SpeechRecognizer
import android.support.v4.app.ActivityCompat
import android.widget.*
import com.github.zagum.speechrecognitionview.adapters.RecognitionListenerAdapter
import com.github.zagum.speechrecognitionview.RecognitionProgressView
import android.Manifest
import android.app.Activity


import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.location.Location
import android.os.AsyncTask
import android.os.Handler

import android.provider.MediaStore
import android.speech.tts.TextToSpeech
import android.util.Base64
import android.view.*
import com.bumptech.glide.Glide

import com.google.android.gms.location.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

import com.mikhaellopez.circularimageview.CircularImageView

import org.jetbrains.anko.alert

import org.jetbrains.anko.startActivity

import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.User
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.util.*
import android.support.v7.app.AlertDialog



class MainActivity : AppCompatActivity() {

    private lateinit var menuBtn: ImageButton
    private lateinit var settingsBtn: ImageButton
    private lateinit var brand: TextView
    private lateinit var topMenu: RelativeLayout
    private lateinit var openMenu: RelativeLayout
    private lateinit var mainLayout: RelativeLayout
    private lateinit var constraintLayout: ConstraintLayout
    private lateinit var userImage: CircularImageView
    private lateinit var userAddImage: ImageButton
    private lateinit var userName: TextView
    private lateinit var userEmail: TextView
    private lateinit var closetBtn: ImageButton
    private lateinit var addClothsBtn: ImageButton
    private lateinit var accountBtn: ImageButton

    private var menuStatus: Boolean = false

    private var speechRecognizer: SpeechRecognizer? = null
    private lateinit var micBtn: Button


    private val RECORD_REQUEST_CODE = 101
    private val LOCATION_REQUEST_CODE = 102

    lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private lateinit var aiDataService: AIDataService
    private lateinit var aiRequest: AIRequest
    private lateinit var textSpeech: TextToSpeech

    private val REQUEST_IMAGE_CAPTURE = 434
    private val PICK_IMAGE = 433

    private lateinit var eventListener: ValueEventListener
    private lateinit var dialog: AlertDialog
    private lateinit var mContext: Context

    private val mAuth = FirebaseAuth.getInstance()
    private val mRef = FirebaseDatabase.getInstance().reference




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        initViews()

        menuBtn.setOnClickListener { showHideMenu() }
        addClothsBtn.setOnClickListener { startActivity<AddClothes>() }
        closetBtn.setOnClickListener { startActivity<Closet>() }
        accountBtn.setOnClickListener { startActivity<Account>() }
        settingsBtn.setOnClickListener { startActivity<Account>() }
        userAddImage.setOnClickListener { showPopupMenu() }




        mContext = this

        val user = mAuth.currentUser?.uid
        eventListener = mRef.child("users").child(user).addValueEventListener(object : ValueEventListener{

            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(data: DataSnapshot?) {
                if (data!!.exists()) {
                    val userData = data.getValue(User::class.java)
                    val photoUrl = userData?.photoUrl
                    val name = userData?.username
                    val email = userData?.email

                    userEmail.text = email

                    if(name != "null"){
                        userName.text = name
                    }


                    if (photoUrl != "null") {
                        if (photoUrl!!.contains("https")) {
                            kotlin.run {
                                Glide.with(applicationContext).load(photoUrl).into(userImage)
                                userImage.setBorderWidth(2F)
                            }


                        } else {
                            val decodeString = Base64.decode(photoUrl, Base64.DEFAULT)
                            val bitmap = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.size)
                            userImage.setImageBitmap(bitmap)
                        }
                    }
                }
            }

        })


        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),LOCATION_REQUEST_CODE)
        }else{


            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

            if(ActivityCompat.checkSelfPermission(this@MainActivity,Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this@MainActivity,Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED)
            {
                requestLocationPermision()
            }
            fusedLocationProviderClient.lastLocation.addOnSuccessListener { location : Location? ->
                val lat = location?.latitude.toString()
                val long = location?.longitude.toString()

            }


        }

        //Speech Recognition

        val colors = intArrayOf(
                ContextCompat.getColor(this, R.color.rhino),
                ContextCompat.getColor(this, R.color.lilac),
                ContextCompat.getColor(this, R.color.wistful),
                ContextCompat.getColor(this, R.color.rhino),
                ContextCompat.getColor(this, R.color.lilac))

        val heights = intArrayOf(40, 44, 38, 43, 36)

        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(this)

        val recognitionProgressView = findViewById<RecognitionProgressView>(R.id.recognition_view)
        recognitionProgressView.setSpeechRecognizer(speechRecognizer)
        recognitionProgressView.setRecognitionListener(object : RecognitionListenerAdapter() {
            override fun onResults(results: Bundle?) {
                showResults(results!!)
                val handler = Handler()
                handler.postDelayed(object: Runnable{
                    override fun run() {
                        recognitionProgressView.stop()
                        recognitionProgressView.play()
                    }
                },3500)


            }

            override fun onEndOfSpeech() {
                Log.d("Speech", "END")
                recognitionProgressView.stop()
                recognitionProgressView.play()
            }
        })

        recognitionProgressView.setColors(colors)
        recognitionProgressView.setBarMaxHeightsInDp(heights)
        recognitionProgressView.play()

        micBtn.setOnClickListener {

            if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED){
                requestAudioPermision()
            }else{
                startRecognition()
                recognitionProgressView.postDelayed(object: Runnable{
                    override fun run() {
                        startRecognition()
                    }
                },50 )
            }
        }

        textSpeech = TextToSpeech(applicationContext, TextToSpeech.OnInitListener { status ->
            if(status == TextToSpeech.SUCCESS){
                textSpeech.language = Locale.US
            } else{
                Log.e("ERROR", "TextToSpeachError")
            }
        })






    }


    override fun onStop() {
        super.onStop()
        finish()
    }

    override fun onResume() {
        super.onResume()

    }

    private fun initViews(){
        menuBtn = findViewById(R.id.menu_btn)
        settingsBtn = findViewById(R.id.settings_btn)
        brand = findViewById(R.id.brand)
        topMenu = findViewById(R.id.top_menu_layout)
        openMenu = findViewById(R.id.open_menu_layout)
        mainLayout = findViewById(R.id.main_layout)
        constraintLayout = findViewById(R.id.main_constraint_layout)
        micBtn = findViewById(R.id.mic_btn)

        //menu views
        userImage = findViewById(R.id.user_image)
        userAddImage = findViewById(R.id.user_add_image)
        userName = findViewById(R.id.name)
        userEmail = findViewById(R.id.email)
        closetBtn = findViewById(R.id.closet_btn)
        addClothsBtn = findViewById(R.id.add_clothes_btn)
        accountBtn = findViewById(R.id.account_btn)


        topMenu.bringToFront()
        mainLayout.bringChildToFront(topMenu)
    }

    private fun showHideMenu(){
        if(!menuStatus) {
            showMenu()
        } else {
            hideMenu()
        }
    }

    private fun startRecognition(){
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, packageName)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "en")
        speechRecognizer?.startListening(intent)
    }

    private fun showResults(results: Bundle) {
        val matches = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
        Toast.makeText(this, matches[0], Toast.LENGTH_LONG).show()

        dialogConfig(matches[0])
    }

    private fun requestAudioPermision() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECORD_AUDIO)) {
            dialog()
        }else{
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.RECORD_AUDIO), RECORD_REQUEST_CODE)
        }
    }

    private fun requestLocationPermision() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            dialog()
        }else{
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_REQUEST_CODE)
        }
    }

    //DialogFlow API
    private fun dialogConfig(matches: String){
        val config = AIConfiguration("de51b8b4ea344e418ce229d51b87922a",
                AIConfiguration.SupportedLanguages.English)

        aiDataService = AIDataService(config)

        aiRequest = AIRequest()

        aiRequest.setQuery(matches)
        aiRequest.sessionId = mAuth?.currentUser?.uid

        dialogAsyncTask().execute(aiRequest)




    }

    internal inner class dialogAsyncTask() : AsyncTask<AIRequest, Void, AIResponse>(){
        override fun onPostExecute(result: AIResponse?) {
            super.onPostExecute(result)
            if (result != null) {
                val aiResponse = result.result

                if(aiResponse.fulfillment.speech != null){
                    textSpeech.speak(aiResponse.fulfillment.speech, TextToSpeech.QUEUE_FLUSH, null)
                }



            }
        }

        override fun doInBackground(vararg requests: AIRequest?): AIResponse? {
            //val request = requests[0]
            try{
                val response = aiDataService.request(aiRequest)
                return response
            } catch (e: AIServiceException){

            }
            return null
        }

    }



    override fun onStart() {
        super.onStart()
        val currentUser = mAuth?.currentUser
        Log.d("User", currentUser.toString())
    }

    private fun showMenu() {
        menuStatus = true
        menuBtn.setImageResource(R.drawable.ic_menu_up_24dp)
        settingsBtn.setImageResource(R.drawable.ic_settings_white_24dp)
        brand.setTextColor(ContextCompat.getColor(this, R.color.zircon))

        val anim = TranslateAnimation(0F,0F,0F,openMenu.height.toFloat())
        anim.duration = 400
        anim.fillAfter = true
        mainLayout.startAnimation(anim)
        mainLayout.elevation = 10F

    }

    private fun hideMenu() {
        menuStatus = false

        val anim = TranslateAnimation(0F,0F,openMenu.height.toFloat(),0F)
        anim.duration = 400
        anim.fillAfter = true
        anim.setAnimationListener(object: Animation.AnimationListener{
            override fun onAnimationStart(animation: Animation?) {
                mainLayout.elevation = 0F
            }

            override fun onAnimationRepeat(p0: Animation?) {
//                not implemented
            }

            override fun onAnimationEnd(p0: Animation?) {
                menuBtn.setImageResource(R.drawable.ic_menu_down_32dp)
                settingsBtn.setImageResource(R.drawable.ic_settings_rhino_24dp)
                brand.setTextColor(resources.getColor(R.color.rhino))
            }
        })
        mainLayout.startAnimation(anim)

    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(menuStatus && event?.y!! > openMenu.height + 25) {
            hideMenu()
        }
        return true
    }

    private fun dialog() {
        val dialog = alert("In order to get all the benefits from SmartClothes app features, you need to grant permission access to your location and microphone services"){
            title = "Permissions needed"
            positiveButton("OK") {requestLocationPermision()}
            negativeButton("CANCEL") {}
        }.show()
        val textView = dialog.findViewById<TextView>(android.R.id.message)
        val typeFace = Typeface.createFromAsset(this.assets, "fonts/SourceSansPro-Regular.ttf")
        textView.typeface = typeFace
    }

    private fun openCamera(){
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
        }
    }

    private fun openGallery(){
        val openGalleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(Intent.createChooser(openGalleryIntent, "Select Picture"), PICK_IMAGE)
    }

    private fun showPopupMenu(){

        val inflater = LayoutInflater.from(this)
        val promtView = inflater.inflate(R.layout.bottom_dialog_image, null)

        val builder = android.support.v7.app.AlertDialog.Builder(this)

        val galleryBtn = promtView.findViewById<LinearLayout>(R.id.gallery_btn)
        val takepicBtn = promtView.findViewById<LinearLayout>(R.id.take_pic_btn)


        galleryBtn.setOnClickListener {
            openGallery()

        }
        takepicBtn.setOnClickListener {
            openCamera()

        }

        builder.setView(promtView)


        dialog = builder.create()

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT)

        val wmlp = dialog.window.attributes
        wmlp.width = WindowManager.LayoutParams.MATCH_PARENT

        dialog.show()

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        dialog.dismiss()

        if (resultCode == Activity.RESULT_OK && data != null){
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                val extras = data.extras
                val imageBitmap = extras!!.get("data") as Bitmap
                userImage.setImageBitmap(imageBitmap)

                try {
                    val baos = ByteArrayOutputStream() //Encode the file as JPG image
                    imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos) // Compress the image to a JPG file with full quality 100
                    val imageEncoded = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)

                    val user = mAuth.currentUser?.uid
                    mRef.child("users").child(user).child("photoUrl").setValue(imageEncoded)

                } catch (e: FileNotFoundException) { //catch the exceptions
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }

            if (requestCode == PICK_IMAGE) {
                val imageUri = data.data
                val imageStream = contentResolver.openInputStream(imageUri)
                val bitmap = BitmapFactory.decodeStream(imageStream)
                userImage.setImageBitmap(bitmap)

                try {
                    val baos = ByteArrayOutputStream() //Encode the file as JPG image
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos) // Compress the image to a JPG file with full quality 100
                    val imageEncoded = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)
                    val user = mAuth.currentUser?.uid
                    mRef.child("users").child(user).child("photoUrl").setValue(imageEncoded)

                } catch (e: FileNotFoundException) { //catch the exceptions
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            } else {
                Toast.makeText(this, getString(R.string.canceled), Toast.LENGTH_SHORT).show()
            }

        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity<MainActivity>()
    }



}







