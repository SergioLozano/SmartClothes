package smartclothesapp.android.smartclothes.es.smartclothes.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Clothes

class PickImageClothAdapter (val mContext: Context?, val mClothes: ArrayList<Clothes?>,  mCategory: ArrayList<String>): RecyclerView.Adapter<PickImageClothAdapter.ViewHolder>() {

    private val clothes: List<Clothes?> = mClothes
    private val category: List<String> = mCategory


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(smartclothesapp.android.smartclothes.es.smartclothes.R.layout.pick_cloth_adapter, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val listClothes = clothes[position]

            Picasso.with(mContext).load(listClothes!!.image).into(holder.image)

    }

    override fun getItemCount(): Int {
        return mClothes.size
    }


    inner class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        var image: ImageButton


        init {
            image = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.user_cloth_image)

        }
    }

}