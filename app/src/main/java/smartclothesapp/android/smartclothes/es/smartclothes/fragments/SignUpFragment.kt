package smartclothesapp.android.smartclothes.es.smartclothes.fragments

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import smartclothesapp.android.smartclothes.es.smartclothes.Utils.EditTextClearCross
import smartclothesapp.android.smartclothes.es.smartclothes.MainActivity


import smartclothesapp.android.smartclothes.es.smartclothes.R
import smartclothesapp.android.smartclothes.es.smartclothes.SessionActivity
import java.util.*


class SignUpFragment : Fragment() {

    var etEmail: EditTextClearCross? = null
    var etPassword: EditTextClearCross? = null
    var etPasswordRepeat: EditTextClearCross? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_sign_up, container, false)

        val signInBtn = view.findViewById<Button>(R.id.to_sign_in_btn)
        val signUpBtn = view.findViewById<Button>(R.id.sign_up_btn)
        etEmail = view?.findViewById(R.id.et_outfit_name)
        etPassword = view?.findViewById(R.id.et_password)
        etPasswordRepeat = view?.findViewById(R.id.et_password_repeat)
        etEmail?.requestFocus()

        signInBtn.setOnClickListener{
            val fragmentManager = fragmentManager
            val fragmentTransaction = fragmentManager!!.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_right)
            fragmentTransaction.addToBackStack(null)
            val signinFragment = SignInFragment()
            fragmentTransaction.replace(R.id.sesionFragments, signinFragment)
            fragmentTransaction.commit()
        }

        signUpBtn.setOnClickListener {
            signUpWithEmailAndPassword()
        }

        return view

    }

    private fun signUpWithEmailAndPassword(){
        val mAuth = FirebaseAuth.getInstance()

        val userMail = etEmail?.text.toString().trim()
        val userPass = etPassword?.text.toString().trim()
        val userPassRepeat = etPasswordRepeat?.text.toString().trim()

        if(userMail.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(userMail).matches()){
            dialog("Account name", "Your account name needs to be a working email address.")
        } else if (userPass.isEmpty() || userPassRepeat.isEmpty() || userPass.length < 8 ){
            dialog("Password error", "Use at least 8 characters, including upper and lower letters, numbers and symbols.")
        } else if (userPass != userPassRepeat){
            dialog("Passwords don't match", "Make sure to type the same password twice.")
        } else {
            mAuth.createUserWithEmailAndPassword(userMail, userPass)
                    .addOnCompleteListener ((activity as SessionActivity),  { task ->
                        if (task.isSuccessful){
                            val db = FirebaseDatabase.getInstance().reference
                            val user = mAuth.currentUser
                            val taskMap = HashMap<String, Any?>()
                            taskMap["id"] = user?.uid
                            taskMap["email"] = user?.email
                            taskMap["username"] = "null"
                            taskMap["photoUrl"] = "null"
                            taskMap["verified"] = user?.isEmailVerified

                            db.child("users").child(user?.uid).setValue(taskMap).addOnCompleteListener {
                                val intent = Intent(activity, MainActivity::class.java)
                                startActivity(intent)
                            }


                        } else {
                            dialog("Error", "An error occurred, please try again later.")
                        }
                    })

        }

    }

    private fun dialog (title: String, message: String) {
        val dialog = AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .show()
        val textView = dialog.findViewById<TextView>(android.R.id.message)
        val typeFace = Typeface.createFromAsset(context?.assets, "fonts/SourceSansPro-Regular.ttf")
        textView.typeface = typeFace
    }
}
