package smartclothesapp.android.smartclothes.es.smartclothes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import org.jetbrains.anko.startActivity
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.provider.MediaStore
import android.util.Base64
import android.view.*
import android.view.LayoutInflater
import com.bumptech.glide.Glide
import com.mikhaellopez.circularimageview.CircularImageView
import org.jetbrains.anko.alert
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.User
import java.io.*


class Account : AppCompatActivity() {

    private lateinit var imageBtn: ImageButton
    private lateinit var userImage: CircularImageView
    private lateinit var dialog: AlertDialog
    private lateinit var aboutBtn: LinearLayout
    private lateinit var manageAccBtn: LinearLayout
    private lateinit var signOutBtn: Button

    private val REQUEST_IMAGE_CAPTURE = 434
    private val PICK_IMAGE = 433

    private val mAuth = FirebaseAuth.getInstance()
    private val mRef = FirebaseDatabase.getInstance().reference
    private lateinit var eventListener: ValueEventListener

    private lateinit var mContext: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        imageBtn = findViewById(R.id.user_add_image)
        userImage = findViewById(R.id.user_image)
        manageAccBtn = findViewById(R.id.manage_account_btn)
        aboutBtn = findViewById(R.id.name_btn)
        signOutBtn = findViewById(R.id.delete_account_btn)

        imageBtn.setOnClickListener { showPopupMenu() }
        manageAccBtn.setOnClickListener { startActivity<ManageAccount>() }
        signOutBtn.setOnClickListener { signOut() }
        aboutBtn.setOnClickListener {  }

        val user = mAuth.currentUser?.uid

        mContext = this

        eventListener = mRef.child("users").child(user).addValueEventListener(object : ValueEventListener{

            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(data: DataSnapshot?) {
                if (data!!.exists()) {
                    val userData = data.getValue(User::class.java)
                    val photoUrl = userData?.photoUrl

                    if (photoUrl != "null") {
                        if (photoUrl!!.contains("https")) {
                            kotlin.run {
                                Glide.with(applicationContext).load(photoUrl).into(userImage)
                                userImage.setBorderWidth(2F)
                            }


                        } else {
                            val decodeString = Base64.decode(photoUrl, Base64.DEFAULT)
                            val bitmap = BitmapFactory.decodeByteArray(decodeString, 0, decodeString.size)
                            userImage.setImageBitmap(bitmap)
                            userImage.setBorderWidth(4F)
                        }
                    }
                }
            }

        })

    }

    override fun onPause() {
        super.onPause()
        mRef.removeEventListener(eventListener)
    }

    override fun onStop() {
        super.onStop()
        mRef.removeEventListener(eventListener)
    }

    private fun signOut(){
        alert (getString(R.string.sign_out_desc)) {
            title = getString(R.string.sign_out)
            positiveButton(getString(R.string.sign_out)) {
                FirebaseAuth.getInstance().signOut()
                startActivity<SessionActivity>()
                finish()
            }
            negativeButton(getString(R.string.cancel)){}
        }.show()

    }



    private fun showPopupMenu(){

        val inflater = LayoutInflater.from(this)
        val promtView = inflater.inflate(R.layout.bottom_dialog_image, null)

        val builder = AlertDialog.Builder(this)

        val galleryBtn = promtView.findViewById<LinearLayout>(R.id.gallery_btn)
        val takepicBtn = promtView.findViewById<LinearLayout>(R.id.take_pic_btn)


        galleryBtn.setOnClickListener {
            openGallery()

        }
        takepicBtn.setOnClickListener {
            openCamera()

        }

        builder.setView(promtView)


        dialog = builder.create()

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window.setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT)

        val wmlp = dialog.window.attributes
        wmlp.width = WindowManager.LayoutParams.MATCH_PARENT

        dialog.show()

    }

    private fun openCamera(){
        val intent = Intent()
        intent.action = MediaStore.ACTION_IMAGE_CAPTURE
        if (intent.resolveActivity(packageManager) != null) {
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
        }
    }

    private fun openGallery(){
        val openGalleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(Intent.createChooser(openGalleryIntent, "Select Picture"), PICK_IMAGE)
    }


     override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

         super.onActivityResult(requestCode, resultCode, data)

         dialog.dismiss()

        if (resultCode == Activity.RESULT_OK && data != null){
         if (requestCode == REQUEST_IMAGE_CAPTURE) {
             val extras = data.extras
             val imageBitmap = extras!!.get("data") as Bitmap
             userImage.setImageBitmap(imageBitmap)
             userImage.setBorderWidth(2F)

             try {
                 val baos = ByteArrayOutputStream() //Encode the file as JPG image
                 imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos) // Compress the image to a JPG file with full quality 100
                 val imageEncoded = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)

                 val user = mAuth.currentUser?.uid
                 mRef.child("users").child(user).child("photoUrl").setValue(imageEncoded)

             } catch (e: FileNotFoundException) { //catch the exceptions
                 e.printStackTrace()
             } catch (e: IOException) {
                 e.printStackTrace()
             }

         }

         if (requestCode == PICK_IMAGE) {
             val imageUri = data.data
             val imageStream = contentResolver.openInputStream(imageUri)
             val bitmap = BitmapFactory.decodeStream(imageStream)
             userImage.setImageBitmap(bitmap)
             userImage.setBorderWidth(2F)

             try {
                 val baos = ByteArrayOutputStream() //Encode the file as JPG image
                 bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos) // Compress the image to a JPG file with full quality 100
                 val imageEncoded = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT)
                 val user = mAuth.currentUser?.uid
                 mRef.child("users").child(user).child("photoUrl").setValue(imageEncoded)

             } catch (e: FileNotFoundException) { //catch the exceptions
                 e.printStackTrace()
             } catch (e: IOException) {
                 e.printStackTrace()
             }
         } else {
             Toast.makeText(this, getString(R.string.canceled), Toast.LENGTH_SHORT).show()
         }

     }

    }
}
