package smartclothesapp.android.smartclothes.es.smartclothes

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Cloth
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.ClothModel
import java.util.*
import com.squareup.picasso.Picasso
import org.jetbrains.anko.alert
import org.jetbrains.anko.indeterminateProgressDialog
import org.jetbrains.anko.startActivity


class ClothDetail : AppCompatActivity(), Observer {

    private var clothId: Long = 0
    private var clothList: java.util.ArrayList<Cloth>? = null

    private lateinit var progressBar: ProgressBar
    private lateinit var removeClothBtn: Button
    private lateinit var image: ImageView
    private lateinit var title: TextView
    private lateinit var desc: TextView
    private lateinit var container: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cloth_detail)
        clothId = intent.getLongExtra("CLOTH_ID", clothId)

        initViews()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp)
        supportActionBar?.setDisplayShowHomeEnabled(true)


        ClothModel

        ClothModel.addObserver(this)

        removeClothBtn.setOnClickListener { removeFromCloset() }


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        startActivity<Closet>()

        return super.onOptionsItemSelected(item)

    }

    override fun onBackPressed() {
        super.onBackPressed()

        startActivity<Closet>()
    }


    override fun update(o: Observable?, arg: Any?) {
        clothList?.clear()
        val data = ClothModel.getData()
        if (data != null) {
            clothList = data
        }
    }

    private fun initViews(){
        progressBar = findViewById(R.id.detail_clothes_progressBar)
        removeClothBtn = findViewById(R.id.delete_outfit_btn)
        image = findViewById(R.id.imageView)
        title = findViewById(R.id.title)
        desc = findViewById(R.id.description)
        container = findViewById(R.id.content_container)
    }

    private fun writeData(){

        container.visibility = View.GONE
        progressBar.visibility = View.VISIBLE

        clothList = ClothModel.getData()

        for (cloth in clothList!!){
            if (cloth.id == clothId.toString()){
                Picasso.with(this)
                        .load(cloth.imageUrl)
                        .placeholder(android.R.drawable.ic_menu_report_image)
                        .error(android.R.drawable.ic_menu_report_image)
                        .into(image)

                title.text = cloth.name
                desc.text = cloth.description

            }
        }

        progressBar.visibility = View.GONE
        container.visibility = View.VISIBLE

    }

    private fun removeFromCloset(){
        val db = FirebaseDatabase.getInstance().reference
        val mAuth = FirebaseAuth.getInstance()
        val user = mAuth.currentUser?.uid
        val dialog = indeterminateProgressDialog(getString(R.string.removing_cloth))
        dialog.show()
        val dbRef = db.child("wardrobe").child(user).child("clothes").child(clothId.toString())
        dbRef.removeValue()
                .addOnCompleteListener(this, { task ->
            if (task.isSuccessful) {
                dialog.dismiss()
                startActivity<Closet>()

            } else {
                dialog.dismiss()
                alert(getString(R.string.delete_error)) {
                    title = getString(R.string.error)
                    positiveButton("OK") {}
                }.show()
            }
        })
    }


    override fun onResume() {
        super.onResume()
        ClothModel.addObserver(this)
        writeData()
    }

    override fun onPause() {
        super.onPause()
        ClothModel.deleteObserver(this)
    }

    override fun onStop() {
        super.onStop()
        ClothModel.deleteObserver(this)
    }
}
