package smartclothesapp.android.smartclothes.es.smartclothes.Adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import smartclothesapp.android.smartclothes.es.smartclothes.ClothDetail
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.WardrobeCloth
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class ClothesStatusAdapter(val mContext: Context?, val layout: Int): RecyclerView.Adapter<ClothesStatusAdapter.ViewHolder>(){

    val user = FirebaseAuth.getInstance().currentUser?.uid
    val mRef = FirebaseDatabase.getInstance().reference.child("wardrobe").child(user).child("clothes")
    var event: ValueEventListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.discard.setOnClickListener {
            resetStatus()
        }

    }

    override fun getItemCount(): Int {
        return 1
    }


    inner class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {

        var discard: Button
        var viewBtn: Button

        init {
            discard = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.discard_btn)
            viewBtn = view.findViewById(smartclothesapp.android.smartclothes.es.smartclothes.R.id.view_btn)

        }
    }

    private fun resetStatus(){


        val statusArray = ArrayList<Int?>()
        statusArray.clear()

        event = mRef.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(snap: DataSnapshot?) {

                val clothList = ArrayList<WardrobeCloth?>()
                val keyList = ArrayList<String>()
                clothList.clear()

                for(data: DataSnapshot in snap!!.children){

                    val cloth = data.getValue(WardrobeCloth::class.java)
                    statusArray.add(cloth?.status)
                    if(cloth?.status != 0){
                        keyList.add(data.key)
                    }



                }

                updateStatus(keyList, event!!)


            }

        })

    }

    private fun updateStatus(keyList: ArrayList<String>, event: ValueEventListener){

        val map= HashMap<String, Any>()

        for (key: String in keyList){
            map.put("status", 0)
            map.put("id", key.toLong())
            mRef.child(key).updateChildren(map).addOnCompleteListener {
                mRef.removeEventListener(event)
            }
        }

    }

}