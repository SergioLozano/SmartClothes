package smartclothesapp.android.smartclothes.es.smartclothes.Utils

import java.math.BigInteger

class NfcUtils {
    companion object {

        private val HEX_CHARS_ARRAY = "0123456789ABCDEF".toCharArray()
        fun toHex(byteArray: ByteArray) : String {
            val result = StringBuffer()

            byteArray.forEach {
                val octet = it.toInt()
                val firstIndex = (octet and 0xF0).ushr(4)
                val secondIndex = octet and 0x0F
                result.append(HEX_CHARS_ARRAY[firstIndex])
                result.append(HEX_CHARS_ARRAY[secondIndex])
            }

            return result.toString()
        }

        fun toDec(s:String):Long {
            val bigInt = BigInteger(s, 16)
            return bigInt.toLong()
        }
    }
}