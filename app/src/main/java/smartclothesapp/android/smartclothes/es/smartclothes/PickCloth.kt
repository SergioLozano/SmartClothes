package smartclothesapp.android.smartclothes.es.smartclothes

import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import smartclothesapp.android.smartclothes.es.smartclothes.Adapters.CategoryClothAdapter
import smartclothesapp.android.smartclothes.es.smartclothes.Adapters.OutfitFormAdapter
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Clothes
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.WardrobeCloth

class PickCloth : AppCompatActivity() {

    private var recyclerSnap: RecyclerView? = null

    private var mUserClothes: ArrayList<Clothes?> = ArrayList()
    private var mCategoryClothAdapter: CategoryClothAdapter? = null

    private lateinit var position: String
    private lateinit var name: String
    private var clothes: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pick_cloth)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_left_white_24dp)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val extras = intent.extras
        if (extras != null){
            position = intent.getStringExtra("position")
            name = intent.getStringExtra("name")
            clothes = intent.getStringArrayListExtra("clothes")
        }

        init()

        getUserClothes()
    }

    private fun init(){
        recyclerSnap = findViewById(R.id.pick_image_recycler)
        val mLayoutManager = GridLayoutManager(this, 4)
        recyclerSnap?.layoutManager = mLayoutManager
        recyclerSnap?.addItemDecoration(GridSpacingItemDecoration(4, dpToPx(10), true))
        mCategoryClothAdapter = CategoryClothAdapter(this, mUserClothes, R.layout.pick_cloth_adapter, position, name, clothes)
        recyclerSnap?.adapter = mCategoryClothAdapter

    }

    private fun getUserClothes(){
        val user = FirebaseAuth.getInstance().currentUser?.uid
        val mRef = FirebaseDatabase.getInstance().reference.child("wardrobe").child(user).child("clothes")
        val userClothsList = ArrayList<WardrobeCloth?>()
        mRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(snap: DataSnapshot?) {
                userClothsList.clear()
                for(data: DataSnapshot in snap!!.children){
                    val cloth = data.getValue(WardrobeCloth::class.java)

                    userClothsList.add(cloth)
                    compareUserClothes(userClothsList)
                }
            }

        })
    }

    private fun compareUserClothes(userClothes: ArrayList<WardrobeCloth?>){
        val mRef = FirebaseDatabase.getInstance().reference.child("clothes").orderByChild("category")

        mRef.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {

            }

            override fun onDataChange(snap: DataSnapshot?) {
                mUserClothes.clear()
                for (data: DataSnapshot in snap!!.children){
                    val cloth = data.getValue(Clothes::class.java)

                    for(getCloth: WardrobeCloth? in userClothes){
                        if(getCloth!!.id == cloth!!.id){
                            mUserClothes.add(cloth)
                        }


                    }

                    mCategoryClothAdapter!!.notifyDataSetChanged()

                }


            }

        })
    }


    inner class GridSpacingItemDecoration(spanCount:Int, spacing:Int, includeEdge:Boolean):RecyclerView.ItemDecoration() {
        private var spanCount:Int = 0
        private var spacing:Int = 0
        private var includeEdge:Boolean = false

        init{
            this.spanCount = spanCount
            this.spacing = spacing
            this.includeEdge = includeEdge
        }

        override fun getItemOffsets(outRect: Rect, view: View, parent:RecyclerView, state:RecyclerView.State) {
            val position = parent.getChildAdapterPosition(view) // item position
            val column = position % spanCount // item column
            if (includeEdge)
            {
                outRect.left = spacing - column * spacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)
                if (position < spanCount)
                { // top edge
                    outRect.top = spacing
                }
                outRect.bottom = spacing // item bottom
            }
            else
            {
                outRect.left = column * spacing / spanCount // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount // spacing - (column + 1) * ((1f / spanCount) * spacing)
                if (position >= spanCount)
                {
                    outRect.top = spacing // item top
                }
            }
        }
    }

    private fun dpToPx(dp: Int): Int {
        val r = resources
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics))
    }
}
