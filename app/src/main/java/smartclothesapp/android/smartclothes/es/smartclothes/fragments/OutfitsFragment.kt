package smartclothesapp.android.smartclothes.es.smartclothes.fragments

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.support.v4.startActivity
import smartclothesapp.android.smartclothes.es.smartclothes.Adapters.OutfitFormAdapter
import smartclothesapp.android.smartclothes.es.smartclothes.DataModel.Outfits
import smartclothesapp.android.smartclothes.es.smartclothes.MainActivity
import smartclothesapp.android.smartclothes.es.smartclothes.ManageAccount
import smartclothesapp.android.smartclothes.es.smartclothes.OutfitForm
import smartclothesapp.android.smartclothes.es.smartclothes.R

class OutfitsFragment : Fragment() {

    private lateinit var recycler: RecyclerView
    private lateinit var fab: FloatingActionButton
    private lateinit var createTv: TextView
    private lateinit var newTv: TextView
    private lateinit var outfitTv: TextView
    private lateinit var arrow: ImageView

    private lateinit var mFirebaseRecycler: FirebaseRecyclerAdapter<Outfits, OutFitViewHolder>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_closet_outfits, container, false)

        fab = view.findViewById(R.id.outfit_fab)
        createTv = view.findViewById(R.id.create_tv)
        newTv = view.findViewById(R.id.new_tv)
        outfitTv = view.findViewById(R.id.outfit_tv)
        arrow = view.findViewById(R.id.arrow)


        fab.setOnClickListener {startActivity<OutfitForm>()}

        arrow.visibility = View.VISIBLE
        createTv.visibility = View.VISIBLE
        newTv.visibility = View.VISIBLE
        outfitTv.visibility = View.VISIBLE

        recycler = view.findViewById(R.id.outfit_recycler)
        val mLayoutManager = LinearLayoutManager(activity)
        recycler.layoutManager = mLayoutManager
        firebaseOutfitsRecycler()

        return view

    }

    override fun onDetach() {
        super.onDetach()
        mFirebaseRecycler.stopListening()
    }


    override fun onPause() {
        super.onPause()
        mFirebaseRecycler.stopListening()
    }

    override fun onStop() {
        super.onStop()
        mFirebaseRecycler.stopListening()
    }

    private fun firebaseOutfitsRecycler(){
        val user = FirebaseAuth.getInstance().currentUser?.uid
        val query = FirebaseDatabase.getInstance().reference.child("wardrobe").child(user).child("outfits").orderByKey()
        val options = FirebaseRecyclerOptions.Builder<Outfits>()
                .setQuery(query, Outfits::class.java)
                .setLifecycleOwner(this)
                .build()


        query.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot?) {
                if (p0!!.childrenCount > 0){
                    arrow.visibility = View.GONE
                    createTv.visibility = View.GONE
                    newTv.visibility = View.GONE
                    outfitTv.visibility = View.GONE
                }
            }

        })


        val adapter = object : FirebaseRecyclerAdapter<Outfits, OutFitViewHolder>(options){
            override fun onBindViewHolder(holder: OutFitViewHolder, position: Int, model: Outfits) {

                holder.name.text = model.name

                val clothesList = ArrayList<String>()

                for (l:Long in model.clothes){
                    clothesList.add(l.toString())
                }

                holder.editOutfitBtn.setOnClickListener {
                    val intent = Intent(activity, OutfitForm::class.java)
                    intent.putExtra("position", model.id)
                    intent.putExtra("name", model.name)
                    intent.putExtra("clothes", clothesList)
                    startActivity(intent)
                }
            }

            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OutFitViewHolder {
                val itemView = LayoutInflater.from(parent.context).inflate(R.layout.outfit_card, parent, false)
                return OutFitViewHolder(itemView)
            }

            override fun onDataChanged() {
                super.onDataChanged()


            }
        }

        mFirebaseRecycler = adapter
        mFirebaseRecycler.startListening()
        recycler.adapter = adapter

    }

    class OutFitViewHolder(mView: View) : RecyclerView.ViewHolder(mView){
        val name: TextView
        val editOutfitBtn: ImageButton

        init {
            this.name = mView.findViewById(R.id.outfit_name)
            this.editOutfitBtn = mView.findViewById(R.id.edit_outfit_btn)
        }

    }



}