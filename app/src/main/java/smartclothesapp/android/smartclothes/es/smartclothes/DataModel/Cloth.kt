package smartclothesapp.android.smartclothes.es.smartclothes.DataModel

import com.google.firebase.database.DataSnapshot

class Cloth (snapshot: DataSnapshot) {
    lateinit var id: String
    lateinit var name: String
    lateinit var imageUrl: String
    lateinit var description: String


    init {
        try {
            val data: HashMap<String, Any> = snapshot.value as HashMap<String, Any>
            id = snapshot.key ?: ""
            name = data["name"] as String
            imageUrl = data["image"] as String
            description = data["description"] as String
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
}